#!/bin/bash

#LOCKFILE=/home/wbadm/triggers-lock
LOCKFILE=/tmp/wbadm-triggers-lock

cleanup() {
    rm -rf "$LOCKFILE"
    rm -rf "$TEMPFILE"
}

#ensure_lock() {
#    if lockfile -! -r 10 -l 6000 "$LOCKFILE"
#    then
#        #echo "Cannot lock $LOCKFILE, aborting."
#        exit 1
#    fi
#}

if ! lockfile -0 -r0 "$LOCKFILE" 2>/dev/null
then
    # Process already running.
    exit 0
fi

# Got the lock.
trap cleanup ERR TERM HUP INT QUIT EXIT

#Triggers for experimental kdepim-ng-exp and kde-frameworks-exp
(cd /srv/wanna-build/triggers && nice -n 19 ./trigger.tritemio-ubuntu)

# Build arch all with amd64.
#psql wannadb -c "update packages set build_arch_all=true where distribution='experimental' and architecture='amd64';"
#psql wannadb -c "update packages set build_arch_all=true where distribution='kdepim-ng-exp' and architecture='amd64';"
#psql wannadb -c "update packages set build_arch_all=true where distribution='kde-frameworks-exp' and architecture='amd64';"

#Build status
TYPES="all kf5 plasma kdeapplications other not-built"
SUITES="ubuntu-exp ubuntu-exp2"

for TYPE in $TYPES; do
	for SUITE in $SUITES; do
		TEMPFILE=`mktemp`
		(cd /home/wbadm/kdenext-build-status/ \
		&& ./kdenext-build-status -d ${SUITE} -r ${TYPE} > ${TEMPFILE} \
		&& cd kdenext_buildstatus_${SUITE} \
		&& mv ${TEMPFILE} ${SUITE}_status_${TYPE}.html \
		&& chmod +r ${SUITE}_status_${TYPE}.html)
	done
done

#Installability status
#(cd /home/wbadm/kdenext-installability-status/ && ./check-installability-kde-frameworks-exp.sh \
#	&& ./check-installability-experimental.sh)

