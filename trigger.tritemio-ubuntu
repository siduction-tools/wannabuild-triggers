#!/bin/bash
# vim:set et ts=4 sw=4 ft=bash ai:

ARCHIVE='tritemio'
SUITES="ubuntu-exp ubuntu-exp2"
ARCHES="i386 amd64"

REPOSITORY='http://gpul.grupos.udc.es/tritemio/dists/'
UBUNTU_REPOSITORY='http://gb.archive.ubuntu.com/ubuntu/dists'
UBUNTU_SUITE='yakkety'

. common

# Redirect the output to a log file
#exec >> /org/wanna-build/db/merge.$ARCHIVE.log 2>&1

#if [ ! -d $REPOSITORY ]
#then
#	echo "Could not find repository $REPOSITORY"
#	exit 1
#fi

if [ -f /org/wanna-build/NO-TRIGGERS ]
then
	echo Trigger for $ARCHIVE skipped due to NO-TRIGGERS, aborting. >&2
	exit 0
fi

echo "`date`: Running trigger for $ARCHIVE ..."

fetch() {
	src="$1"
	dest="$2"
	mkdir -p "$dest"
        cd "$dest"
        wget -N "$src"
	cd -
}

main() {
	set -eE
	trap cleanup ERR TERM HUP INT QUIT

	ensure_lock
	ensure_workdir

	# Fetch the most recent Packages and Sources files.
	for suite in $SUITES
	do
		fetch "$REPOSITORY/$suite/main/source/Sources.gz" "$ARCHIVE_BASE/archive/$suite/main/source/"
		zcat "$ARCHIVE_BASE/archive/$suite/main/source/Sources.gz" \
			| sed 's/^Architecture: all/Architecture: amd64/' \
			> "$ARCHIVE_BASE/archive/$suite/main/source/Sources"
		rm "$ARCHIVE_BASE/archive/$suite/main/source/Sources.gz"
		gzip -f "$ARCHIVE_BASE/archive/$suite/main/source/Sources"
		#fetch "$REPOSITORY/$suite/contrib/source/Sources.gz" "$ARCHIVE_BASE/archive/$suite/contrib/source/"
		
		for arch in $ARCHES
		do
			# fetch kdenext
			fetch "$REPOSITORY/$suite/main/binary-$arch/Packages.gz" \
				"$ARCHIVE_BASE/archive/$suite/main/binary-$arch/siduction/"

			# fetch ubuntu main
			fetch "$UBUNTU_REPOSITORY/$UBUNTU_SUITE/main/binary-$arch/Packages.gz" \
				"$ARCHIVE_BASE/archive/$suite/main/binary-$arch/ubuntu/"
			# fetch ubuntu main (proposed)
			fetch "$UBUNTU_REPOSITORY/$UBUNTU_SUITE-proposed/main/binary-$arch/Packages.gz" \
				"$ARCHIVE_BASE/archive/$suite-proposed/main/binary-$arch/ubuntu/"

			#fetch ubuntu universe
			fetch "$UBUNTU_REPOSITORY/$UBUNTU_SUITE/universe/binary-$arch/Packages.gz" \
				"$ARCHIVE_BASE/archive/$suite/universe/binary-$arch/ubuntu/"
			#fetch ubuntu universe (proposed)
			fetch "$UBUNTU_REPOSITORY/$UBUNTU_SUITE-proposed/universe/binary-$arch/Packages.gz" \
				"$ARCHIVE_BASE/archive/$suite-proposed/universe/binary-$arch/ubuntu/"

			#fetch silo 16
			#fetch "http://ppa.launchpad.net/ci-train-ppa-service/landing-016/ubuntu/dists/$UBUNTU_SUITE/main/binary-$arch/Packages.gz" \
			#	"$ARCHIVE_BASE/archive/$suite/main/binary-$arch/ubuntu-landing-016/"
			#fetch silo 39
			#fetch "http://ppa.launchpad.net/ci-train-ppa-service/landing-039/ubuntu/dists/$UBUNTU_SUITE/main/binary-$arch/Packages.gz" \
			#	"$ARCHIVE_BASE/archive/$suite/main/binary-$arch/ubuntu-landing-039/"

			# Add kdenext to the final packages list
			zcat "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/siduction/Packages.gz" \
				> "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/Packages"

			# Add ubuntu main to the final packages list
			zcat "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/ubuntu/Packages.gz" \
				| grep -v ^Python_version: \
				>> "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/Packages"
			# Add ubuntu main to the final packages list (proposed)
			zcat "$ARCHIVE_BASE/archive/$suite-proposed/main/binary-$arch/ubuntu/Packages.gz" \
				| grep -v ^Python_version: \
				>> "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/Packages"

			# Add ubuntu universe to the final packages list
			zcat "$ARCHIVE_BASE/archive/$suite/universe/binary-$arch/ubuntu/Packages.gz" \
				| grep -v ^Python_version: \
				>> "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/Packages"
			# Add ubuntu universe to the final packages list (proposed)
			zcat "$ARCHIVE_BASE/archive/$suite-proposed/universe/binary-$arch/ubuntu/Packages.gz" \
				| grep -v ^Python_version: \
				>> "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/Packages"

			# Add silo 16 to the final packages list
			#zcat "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/ubuntu-landing-016/Packages.gz" \
			#	| grep -v ^Python_version: \
			#	>> "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/Packages"
			# Add silo 39 to the final packages list
			#zcat "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/ubuntu-landing-039/Packages.gz" \
			#	| grep -v ^Python_version: \
			#	>> "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/Packages"
			
			# Compress the final list of packages.
			gzip -f "$ARCHIVE_BASE/archive/$suite/main/binary-$arch/Packages"
		done
	done

	for suite in $SUITES
	do
		SOURCES="Sources.$suite.incoming-filtered.gz"
		filter_out_nonfree "$ARCHIVE_BASE/archive/$suite/main/source/Sources.gz" "$SOURCES"
		PACKAGES="$ARCHIVE_BASE/archive/$suite/main/binary-%ARCH%/Packages.gz"
		trigger_wb_update "$suite" "$ARCHES" "$SOURCES" "$PACKAGES"
		#PACKAGES="$ARCHIVE_BASE/archive/$suite/main/binary-all/Packages.gz"
		#trigger_wb_update "$suite" "all" "$SOURCES" "$PACKAGES"
	done

	cleanup
}

main
exit 0

